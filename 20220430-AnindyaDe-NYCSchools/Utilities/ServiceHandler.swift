//
//  ServiceHandler.swift
//  20220430-AnindyaDe-NYCSchools
//
//  Created by Anindya on 5/1/22.
//

import Foundation
class ServiceHandler {
    
    static let sharedInstance = ServiceHandler()
    private init(){
        
    }
    ///Function will help you in fetching the service response in the form of Result using below parameters
        /// - Parameters:
        ///   - urlString:  url in string format.
        ///   - codableClassName : Geniric Decodable parameter,Pass codable class name to perform decoding
        /// - completionHandler : clouser response handler will return you Result containing either Success Response or Error
    func fetchData<Element: Decodable>(className: Element.Type, APIName: URL, completion: @escaping (Any?) -> ()){
        
        URLSession.shared.dataTask(with: APIName){ (data, urlResponse, error) in
            if let httpResponse = urlResponse as? HTTPURLResponse {
                if httpResponse.statusCode != 200 {
                    print("Error: not successfull!")
                    completion([])
                    return
                }
            }
            
            if let data = data {
                let schoolData = try! JSONDecoder().decode(className, from: data)
                completion(schoolData)
            }else{
                completion(nil)
            }
        }.resume()
    }
    
}
