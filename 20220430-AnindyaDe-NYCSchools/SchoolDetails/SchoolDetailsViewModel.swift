//
//  SchoolDetailsViewModel.swift
//  20220430-AnindyaDe-NYCSchools
//
//  Created by Anindya on 5/1/22.
//

import Foundation

enum FetchState {
    case loading
    case loaded([String])
    case empty
    case error
}

class SchoolDetailsViewModel {
    
    init(service: ServiceHandler){
        self.service = service
    }
    var state : ((FetchState)->())?
    var service: ServiceHandler?
}

extension SchoolDetailsViewModel: SchoolDetailsService {
    
    func loadDetailsData(primaryKey: String) {
       print(primaryKey)
        self.state?(.loading)
        getSchoolDetails(dbn: primaryKey) { [weak self] data in
            guard let data = data else{
                self?.state?(.error)
                return
            }
            if (data as AnyObject).count < 0 {
                self?.state?(.empty)
                return
            }
            self?.state?(.loaded((self?.formedDataSorce(data: data))!))
            return
        }
    }


    func formedDataSorce(data: [SchoolDetailsModel]) -> [ String] {
        if data.count > 0{
            let schDtls = data[0];
            var source = [""]
        
            source = [ "\(schDtls.school_name ?? "")",
                   "Test Taker: \(schDtls.num_of_sat_test_takers ?? "")",
                   "Critical Reading Average: \(schDtls.sat_critical_reading_avg_score ?? "")",
                   "Math Average: \(schDtls.sat_math_avg_score ?? "")",
                   "Writing Average: \(schDtls.sat_writing_avg_score ?? "")" ]
       
            return source
        }else{
            return [""]
        }
    }
}




protocol SchoolDetailsService {
    func getSchoolDetails(dbn: String, completion: @escaping ([SchoolDetailsModel]?) -> ())
}

extension SchoolDetailsService {
    
    func getSchoolDetails(dbn: String, completion: @escaping ([SchoolDetailsModel]?) -> ()){
        
        let URLStr = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json?dbn=\(dbn)"
        let schoolDetailsAPI = URL(string:URLStr)
        let service = ServiceHandler.sharedInstance
        service.fetchData(className: Array<SchoolDetailsModel>.self, APIName: schoolDetailsAPI!) { data in
            
            if let data = data {
                completion(data as? [SchoolDetailsModel])
            }else{
                completion([SchoolDetailsModel]())
            }
        }
    }
}
