//
//  SchoolModel.swift
//  20220430-AnindyaDe-NYCSchools
//
//  Created by Anindya on 5/1/22.
//

import Foundation

struct SchoolModel : Codable {
    let dbn : String?
    let school_name : String?
    let boro : String?
    let overview_paragraph : String?
    let school_10th_seats : String?
    let academicopportunities1 : String?
    let academicopportunities2 : String?
    let ell_programs : String?
    let neighborhood : String?
    let building_code : String?
    let location : String?
    let phone_number : String?
    let fax_number : String?
    let school_email : String?
    let website : String?
    let subway : String?
    let bus : String?
    let grades2018 : String?
    let finalgrades : String?
    let total_students : String?
    let extracurricular_activities : String?
    let school_sports : String?
    let attendance_rate : String?
    let pct_stu_enough_variety : String?
    let pct_stu_safe : String?
    let school_accessibility_description : String?
    let directions1 : String?
    let requirement1_1 : String?
    let requirement2_1 : String?
    let requirement3_1 : String?
    let requirement4_1 : String?
    let requirement5_1 : String?
    let offer_rate1 : String?
    let program1 : String?
    let code1 : String?
    let interest1 : String?
    let method1 : String?
    let seats9ge1 : String?
    let grade9gefilledflag1 : String?
    let grade9geapplicants1 : String?
    let seats9swd1 : String?
    let grade9swdfilledflag1 : String?
    let grade9swdapplicants1 : String?
    let seats101 : String?
    let admissionspriority11 : String?
    let admissionspriority21 : String?
    let admissionspriority31 : String?
    let grade9geapplicantsperseat1 : String?
    let grade9swdapplicantsperseat1 : String?
    let primary_address_line_1 : String?
    let city : String?
    let zip : String?
    let state_code : String?
    let latitude : String?
    let longitude : String?
    let community_board : String?
    let council_district : String?
    let census_tract : String?
    let bin : String?
    let bbl : String?
    let nta : String?
    let borough : String?

    private enum CodingKeys: String, CodingKey {

        case dbn = "dbn"
        case school_name = "school_name"
        case boro = "boro"
        case overview_paragraph = "overview_paragraph"
        case school_10th_seats = "school_10th_seats"
        case academicopportunities1 = "academicopportunities1"
        case academicopportunities2 = "academicopportunities2"
        case ell_programs = "ell_programs"
        case neighborhood = "neighborhood"
        case building_code = "building_code"
        case location = "location"
        case phone_number = "phone_number"
        case fax_number = "fax_number"
        case school_email = "school_email"
        case website = "website"
        case subway = "subway"
        case bus = "bus"
        case grades2018 = "grades2018"
        case finalgrades = "finalgrades"
        case total_students = "total_students"
        case extracurricular_activities = "extracurricular_activities"
        case school_sports = "school_sports"
        case attendance_rate = "attendance_rate"
        case pct_stu_enough_variety = "pct_stu_enough_variety"
        case pct_stu_safe = "pct_stu_safe"
        case school_accessibility_description = "school_accessibility_description"
        case directions1 = "directions1"
        case requirement1_1 = "requirement1_1"
        case requirement2_1 = "requirement2_1"
        case requirement3_1 = "requirement3_1"
        case requirement4_1 = "requirement4_1"
        case requirement5_1 = "requirement5_1"
        case offer_rate1 = "offer_rate1"
        case program1 = "program1"
        case code1 = "code1"
        case interest1 = "interest1"
        case method1 = "method1"
        case seats9ge1 = "seats9ge1"
        case grade9gefilledflag1 = "grade9gefilledflag1"
        case grade9geapplicants1 = "grade9geapplicants1"
        case seats9swd1 = "seats9swd1"
        case grade9swdfilledflag1 = "grade9swdfilledflag1"
        case grade9swdapplicants1 = "grade9swdapplicants1"
        case seats101 = "seats101"
        case admissionspriority11 = "admissionspriority11"
        case admissionspriority21 = "admissionspriority21"
        case admissionspriority31 = "admissionspriority31"
        case grade9geapplicantsperseat1 = "grade9geapplicantsperseat1"
        case grade9swdapplicantsperseat1 = "grade9swdapplicantsperseat1"
        case primary_address_line_1 = "primary_address_line_1"
        case city = "city"
        case zip = "zip"
        case state_code = "state_code"
        case latitude = "latitude"
        case longitude = "longitude"
        case community_board = "community_board"
        case council_district = "council_district"
        case census_tract = "census_tract"
        case bin = "bin"
        case bbl = "bbl"
        case nta = "nta"
        case borough = "borough"
    }

}

