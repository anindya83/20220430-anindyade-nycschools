//
//  SchoolViewModel.swift
//  20220430-AnindyaDe-NYCSchools
//
//  Created by Anindya on 5/1/22.
//

import Foundation

enum State <T> {
    case loading
    case loaded([SchoolModel])
    case empty
    case error
}

class SchoolViewModel {
    
    init(service: ServiceHandler){
        self.service = service
    }
    var state : ((State<Any>)->())?
    var service: ServiceHandler?
    var title = "NYC Schools"
}

extension SchoolViewModel: SchoolService {
    
    func loadData() {
        self.state?(.loading)
        getSchoolList { [weak self] data in
            guard let data = data else{
                self?.state?(.error)
                return
            }
            if data.count < 1 {
                self?.state?(.empty)
            }
            self?.state?(.loaded(data))
            return
        }
    }
}

protocol SchoolService {
    func getSchoolList(completion: @escaping ([SchoolModel]?) -> ())
}

extension SchoolService {
    
    func getSchoolList(completion: @escaping ([SchoolModel]?) -> ()){
        
        let schoolListAPI = URL(string: "https://data.cityofnewyork.us/resource/s3k6-pzi2.json")
        let service = ServiceHandler.sharedInstance
        service.fetchData(className: Array<SchoolModel>.self, APIName: schoolListAPI!) { data in
            
            if let data = data {
                completion(data as? [SchoolModel])
            }else{
                completion([SchoolModel]())
            }
        }
    }
}
