#  Anindya De

Service Layer :- Implemented a separate service layer which can be easly accessed across the project. Used Codeable to parse the JSON response. 

Schools List:
Service Handler :-  Idea is to call the servcie layer via service helper class so that code can be organised for each and every servcie call. This will only have reqeust and response formation.

TableView:- Implemented Table view to display all schools 

ViewModel:- This class will not have any UI components only used to get all the UI elements ready.

State of the Task is maintained to so that for differnt operation can be handled i.e isEmpty,loading,loaded etc.

I have more concentrated on writing clean code in swift.

I have used a design patterns to make code clean and implement MVVM. maintained separate service layer on top of it. I wrote servcie handler classes with protocol extension so that in future it can be use easily .
