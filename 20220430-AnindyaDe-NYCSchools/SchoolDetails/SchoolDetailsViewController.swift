//
//  SchoolDetailsViewController.swift
//  20220430-AnindyaDe-NYCSchools
//
//  Created by Anindya on 5/1/22.
//

import UIKit
import MapKit

private let reuseCellIdentifier1 = "SchoolDetailsTableCell"
class SchoolDetailsViewController: UIViewController {

    var primaryKey : String = ""
    
    @IBOutlet weak var tableView : UITableView!

    var detailVM: SchoolDetailsViewModel?
    //var dataSource : [SchoolDetailsModel]?
    var dataSource : [String]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
       // loadSchoolDetails(pKey: primaryKey)
    }
    
    func loadSchoolDetails(pKey: String) {
        primaryKey = pKey;
        print(primaryKey )
        
        detailVM = SchoolDetailsViewModel(service: ServiceHandler.sharedInstance)
        detailVM?.loadDetailsData(primaryKey: (primaryKey ))

        detailVM?.state = { [weak self] state in
            switch state {
            case .loading:
                break
            case .loaded(let datasource):
                DispatchQueue.main.async {
                    self?.title = datasource.first
                    self?.dataSource = datasource
                    self?.dataSource?.remove(at: 0)
                    self?.tableView.reloadData()
                }
                break
            case .empty:
                break
            case .error:
                break
            }
        }
    }
}


extension SchoolDetailsViewController : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseCellIdentifier1) as! SchoolDetailsTableViewCell
        cell.setDataSource(dataSource: dataSource![indexPath.row])
        return cell

    }
}
