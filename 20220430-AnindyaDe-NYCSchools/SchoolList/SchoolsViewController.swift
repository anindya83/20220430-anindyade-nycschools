//
//  SchoolsViewController.swift
//  20220430-AnindyaDe-NYCSchools
//
//  Created by Anindya on 5/1/22.
//

import UIKit

private let reuseCellIdentifier = "SchoolListCell"

class SchoolsViewController: UIViewController {

    @IBOutlet weak var tableView : UITableView!
    var schoolViewModel: SchoolViewModel?
    var primaryKey : String?
    
    lazy var vm : SchoolViewModel? = {
        let schoolViewModel = SchoolViewModel(service: ServiceHandler.sharedInstance)
        schoolViewModel.loadData()
        return schoolViewModel
    }()
    
    var dataSource : [SchoolModel]?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = vm?.title
        
        vm?.state = { [weak self] state in
            switch state {
            case .loading:
                break
            case .loaded(let datasource):
                DispatchQueue.main.async {
                    self?.dataSource = datasource
                    self?.tableView.reloadData()
                }
                break
            case .empty:
                break
            case .error:
                break
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //var t = sender as? String ?? ""
        
        if let pKey = sender as? String {
            let detailsView = segue.destination as? SchoolDetailsViewController
          //  detailsView?.view.tag = 0
           // detailsView?.primaryKey = schoolObject.dbn ?? ""
            detailsView?.loadSchoolDetails(pKey: pKey )
            print(detailsView?.primaryKey ?? "")
       }
        
        
    }
}

extension SchoolsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseCellIdentifier) as! SchoolTableViewCell
        cell.setDataSource(dataSource:(dataSource?[indexPath.row])!)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        primaryKey = (dataSource?[indexPath.row].dbn)
        self.performSegue(withIdentifier: "listToDetailSegue", sender: primaryKey)
        
    }
    
}
