//
//  SchoolDetailsTableViewCell.swift
//  20220430-AnindyaDe-NYCSchools
//
//  Created by Anindya on 5/1/22.
//

import UIKit

class SchoolDetailsTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel : UILabel?
    @IBOutlet weak var valueLabel : UILabel?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    public func setDataSource(dataSource: String) {
        self.titleLabel?.text = dataSource
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
