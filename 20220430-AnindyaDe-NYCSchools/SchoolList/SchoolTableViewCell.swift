//
//  SchoolTableViewCell.swift
//  20220430-AnindyaDe-NYCSchools
//
//  Created by Anindya on 5/1/22.
//

import UIKit

class SchoolTableViewCell: UITableViewCell {

    @IBOutlet weak var schoolNameLabel : UILabel?
    @IBOutlet weak var schoolAddress : UILabel?
    @IBOutlet weak var schoolPhone : UIButton?
    @IBOutlet weak var schoolLatLong : UIButton?
    
    var schoolObj = SchoolModel?.self {
        didSet {
            //schoolNameLabel?.text = schoolObj.
        }
    }
    
    public func setDataSource(dataSource: SchoolModel) {
        self.schoolNameLabel?.text = dataSource.school_name
        self.schoolAddress?.text = dataSource.primary_address_line_1
        self.schoolPhone?.setTitle(dataSource.phone_number, for: .normal)
        //self.schoolLatLong?.setTitle("Navigate", for: .normal)

    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
